<?php
    namespace App\Model;

    class TransferenciaModel
    {
        private $nomeRemetente;
        private $numContaRem;
        private $valorTransferencia;
        private $nomeDestinatario;
        private $numContaDest;
        private $dataTransacao;

        public function setNomeRemetente($valor)
        {
            $this->nomeRemetente = $valor;
        }

        public function setNumContaRem($valor)
        {
            $this->numContaRem = $valor;
        }

        public function setValorTransf($valor)
        {
            $this->valorTransferencia = $valor;
        }

        public function setNomeDest($valor)
        {
            $this->nomeDestinatario = $valor;
        }

        public function setNumContaDest($valor)
        {
            $this->numContaDest = $valor;
        }

        public function setDataTransf($valor)
        {
            $this->dataTransacao= $valor;
        }

        public function getNomeRemetente()
        {
            return $this->nomeRemetente;
        }

        public function getNumContaRem()
        {
            return $this->numContaRem;
        }

        public function getValorTransf()
        {
            return $this->valorTransferencia;
        }

        public function getNomeDest()
        {
            return $this->nomeDestinatario;
        }

        public function getNumContaDest()
        {
            return $this->numContaDest;
        }

        public function getDataTransf()
        {
            return $this->dataTransacao;
        }
    }
?>