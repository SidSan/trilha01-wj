<?php
    namespace App\Model;
    require_once '../../Config.php';
    use LoginController;
    
    class ValidaLogin
    {
        public function valida(LoginController $c)
        {
            $login = $c->getLogin();
            $senha = $c->getSenha();

            $query  = "SELECT * FROM login_correntista WHERE cpf_cnpj =:usuario LIMIT 1";
	        $stmt = Conexao::getCon()->prepare($query);

            $stmt->bindParam(':usuario', $login, \PDO::PARAM_STR);

            if($stmt->execute())
            {
                if(($linha = $stmt->fetch(\PDO::FETCH_ASSOC)))
                {
                    if(password_verify($senha, $linha['senha']))
                    {
                        $_SESSION['cliente_autenticado'] = true;
                        $_SESSION['id_cliente'] = $linha['id_correntista'];
                        die("logado");
                    }
                    else
                    {                     
                        session_destroy();
                        $_SESSION['cliente_autenticado'] = false;                   
                    }
                }
            }
        }
    }
?>