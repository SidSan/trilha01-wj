<?php
    namespace App\Model;
    use TransferenciaController;

    require_once '../../Config.php';

    class CorrentistaDao
    {
        public function create(Correntista $c)
        {
            $sql = 'INSERT INTO correntista (nome_razao_social, email, rg_inscricao_social, data_nasc_fundacao, telefone, cep, cidade, bairro, rua, num_rua, complemento, num_conta, saldo)
                    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';

            $stmt = Conexao::getCon()->prepare($sql);
            $stmt->bindValue(1, $c->getNomeRazao());
            $stmt->bindValue(2, $c->getEmail());
            $stmt->bindValue(3, $c->getRgInscSocial());
            $stmt->bindValue(4, $c->getData());
            $stmt->bindValue(5, $c->getTelefone());
            $stmt->bindValue(6, $c->getCep());
            $stmt->bindValue(7, $c->getCidade());
            $stmt->bindValue(8, $c->getBairro());
            $stmt->bindValue(9, $c->getRua());
            $stmt->bindValue(10, $c->getNumRua());
            $stmt->bindValue(11, $c->getComplemento());
            $stmt->bindValue(12, $c->getNumConta());
            $stmt->bindValue(13, $c->getSaldo());

            if($stmt->execute())
            {
                $lastId = Conexao::getCon()->lastInsertId();

                $sql2 = 'INSERT INTO login_correntista (id_correntista, cpf_cnpj, senha, data_cadastro)
                        VALUES (?, ?, ?, ?)';
                
                $stmt2 = Conexao::getCon()->prepare($sql2);
                $stmt2->bindValue(1, $lastId);
                $stmt2->bindValue(2, $c->getCpfCnpj());
                $stmt2->bindValue(3, $c->getSenha());
                $stmt2->bindValue(4, $c->getDataCadastro());

                if(!$stmt2->execute())
                    throw new $stmt2->errorInfo(); die();
            }
            else
            {
                throw new $stmt->errorInfo();
                die();
            }
        }

        public function saldo()
        {
            $sql = 'SELECT * FROM correntista WHERE id_cliente =:id';
            $stmt = Conexao::getCon()->prepare($sql);

            $stmt->bindParam(':id', $_SESSION['id_cliente'], \PDO::PARAM_STR);

            if($stmt->execute())
            {
                if(($linha = $stmt->fetch(\PDO::FETCH_ASSOC)))
                {
                    $saldo = $linha['saldo'];
                }
                else
                {
                    die("sem registro");
                }
            }
            else
                die("n executou");

            return $saldo;
        }

        public function saque(SaquesController $sc)
        {
           $valorSaque = $sc->getValorSaque();
           $correntistaDao = new CorrentistaDao();
           $saldoAtual = $correntistaDao->saldo();
        
            if($valorSaque > $saldoAtual)
            {
                echo "Saldo insuficiente";
                die;
            }
            else
            {      
                $sql = 'UPDATE correntista SET saldo= saldo - ? WHERE id_cliente=?';
                $stmt= Conexao::getCon()->prepare($sql);

                if($stmt->execute([$valorSaque, $_SESSION['id_cliente']]))
                    die("sucesso");
                else
                    die('sem sucesso');
            }
           
        }

        public function deposito(DepositoController $dc)
        {
            $valorDeposito = $dc->getValorDeposito();       

            $sql = 'UPDATE correntista SET saldo= saldo + ? WHERE id_cliente=?';
            $stmt= Conexao::getCon()->prepare($sql);

            if($stmt->execute([$valorDeposito, $_SESSION['id_cliente']]))
                die("sucesso");
            else
                die('sem sucesso'); 
        }

        public function transferencia(TransferenciaController $t)
        {
            $transferencia = false;
            $valorTransf = $t->getValorTransf();
            $correntistaDao = new CorrentistaDao();
            $saldoAtual = $correntistaDao->saldo();

            if($valorTransf > $saldoAtual)
            {
                die("saldo insuficiente");
            }
            else
            {
                $sql  = 'UPDATE correntista SET saldo= saldo - ? WHERE id_cliente=?';
                $stmt = Conexao::getCon()->prepare($sql);

                if($stmt->execute([$valorTransf, $_SESSION['id_cliente']]))
                {
                    $sql2  = 'UPDATE correntista SET saldo= saldo + ? WHERE num_conta=?';
                    $stmt2 = Conexao::getCon()->prepare($sql2);

                    if($stmt2->execute([$valorTransf, $t->getNumContaDest()]))
                    {
                        //die("tudo foi um sucesso");
                        $transferencia = true;
                    }
                    else
                    {
                        die("deu ruim na parte do destinatario");
                    }
                }
                else
                {
                    die("tirar grana do remetente falhou");
                }
            }

            if($transferencia == true)
            {
                unset($stmt, $sql);

                $sql = 'INSERT INTO transacoes (nome_remetente, num_conta_rem, valor_transferencia, id_remetente, nome_destinatario, num_conta_dest, data_transacao)
                        VALUES (?, ?, ?, ?, ?, ?, ?)';
                
                $stmt = Conexao::getCon()->prepare($sql);
                $stmt->bindValue(1, $t->getNomeRemetente());
                $stmt->bindValue(2, $t->getNumContaRem());
                $stmt->bindValue(3, $t->getValorTransf());
                $stmt->bindValue(4, $_SESSION['id_cliente']);
                $stmt->bindValue(5, $t->getNomeDest());
                $stmt->bindValue(6, $t->getNumContaDest());
                $stmt->bindValue(7, $t->getDataTransf());

                if($stmt->execute())
                {
                    die("inserido em transacao com sucesso");
                }
                else
                {
                    die("deu ruim na parte da tabela transacao");
                }
            }
        }
    }
?>