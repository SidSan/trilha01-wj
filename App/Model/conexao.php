<?php

    namespace App\Model;

    class Conexao {
        private static $instancia;

        public static function getCon()
        {
            if(!isset(self::$instancia))
            {
                self::$instancia = new \PDO('mysql:host=localhost;dbname=trilha01;','root','');
            }   

            return self::$instancia;
        }
    }
?>