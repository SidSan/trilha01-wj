<?php
    require_once '../../vendor/autoload.php';
    require_once '../../Config.php';

    $transferenciaModel = new \App\Model\TransferenciaModel();

    class TransferenciaController
    {
        private $nomeRemetente;
        private $numContaRem;
        private $valorTransferencia;
        private $nomeDestinatario;
        private $numContaDest;
        private $dataTransacao;

        public function __construct()
        {
            $this->nomeRemetente      = isset($_POST['nomeRemetente']) ? $_POST['nomeRemetente'] : "teste";
            $this->numContaRem        = isset($_POST['numContaRem']) ? $_POST['numContaRem'] : 123456789;
            $this->valorTransferencia = isset($_POST['valorTransferencia']) ? $_POST['valorTransferencia'] : "teste";
            $this->nomeDestinatario   = isset($_POST['nomeDestinatario']) ? $_POST['nomeDestinatario'] : "teste";
            $this->numContaDest       = isset($_POST['numContaDest']) ? $_POST['numContaDest'] : "teste";
            $this->dataTransacao      = isset($_POST['dataTransacao']) ? $_POST['dataTransacao'] : date('d-m-Y H:i:s');
        }

        public function getNomeRemetente()
        {
            return $this->nomeRemetente;
        }

        public function getNumContaRem()
        {
            return $this->numContaRem;
        }

        public function getValorTransf()
        {
            return $this->valorTransferencia;
        }

        public function getNomeDest()
        {
            return $this->nomeDestinatario;
        }

        public function getNumContaDest()
        {
            return $this->numContaDest;
        }

        public function getDataTransf()
        {
            return $this->dataTransacao;
        }
    }

    $transferenciaController = new TransferenciaController();

    $transferenciaModel->setNomeRemetente($transferenciaController->getNomeRemetente());
    $transferenciaModel->setNumContaRem($transferenciaController->getNumContaRem());
    $transferenciaModel->setValorTransf($transferenciaController->getValorTransf());
    $transferenciaModel->setNomeDest($transferenciaController->getNomeDest());
    $transferenciaModel->setNumContaDest($transferenciaController->getNumContaDest());
    $transferenciaModel->setDataTransf($transferenciaController->getDataTransf());

    $correntistaDao = new \App\Model\CorrentistaDao();
    $correntistaDao->transferencia($transferenciaController);
?>