<?php
    require_once '../../vendor/autoload.php';

    class LoginController
    {
        private $login;
        private $senha;

        public function __construct()
        {   
            $this->login = isset($_POST['usuario']) ? $_POST['usuario'] : null;
            $this->senha = isset($_POST['senha']) ? $_POST['senha'] : null;
        }

        public function getLogin()
        {
            return $this->login;
        }

        public function getSenha()
        {
            return $this->senha;
        }
    }

    $loginControl = new LoginController();

    $validaLogin = new \App\Model\ValidaLogin();
    $validaLogin->valida($loginControl);
?>