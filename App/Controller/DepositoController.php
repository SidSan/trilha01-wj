<?php
    namespace App\Model;
    require_once '../../vendor/autoload.php';

    class DepositoController
    {
        private $quantia;

        public function __construct()
        {
            $this->quantia = $_POST['quantia'];
        }

        public function getValorDeposito()
        {
            return $this->quantia;
        }
    }

    $depositoController = new DepositoController();

    $correntistaDao = new \App\Model\CorrentistaDao();
    $correntistaDao->deposito($depositoController);
?>
