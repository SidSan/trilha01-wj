<?php
    namespace App\Model;
    require_once '../../vendor/autoload.php';

    class SaquesController
    {
        private $quantia;

        public function __construct()
        {
            $this->quantia = $_POST['quantia'];
        }

        public function getValorSaque()
        {
            return $this->quantia;
        }
    }

    $saquesController = new SaquesController();

    $saqueModel = new \App\Model\CorrentistaDao();
    $saqueModel->saque($saquesController);
?>