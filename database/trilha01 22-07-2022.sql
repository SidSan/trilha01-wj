-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 22-Jul-2022 às 20:30
-- Versão do servidor: 10.4.24-MariaDB
-- versão do PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `trilha01`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `correntista`
--

CREATE TABLE `correntista` (
  `id_cliente` int(11) NOT NULL,
  `nome_razao_social` varchar(150) NOT NULL,
  `email` varchar(320) NOT NULL,
  `rg_inscricao_social` varchar(10) NOT NULL,
  `data_nasc_fundacao` date NOT NULL,
  `telefone` varchar(15) NOT NULL,
  `cep` varchar(9) NOT NULL,
  `cidade` varchar(100) NOT NULL,
  `bairro` varchar(100) NOT NULL,
  `rua` varchar(100) NOT NULL,
  `num_rua` int(5) NOT NULL,
  `complemento` varchar(150) DEFAULT NULL,
  `num_conta` int(10) NOT NULL,
  `saldo` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `correntista`
--

INSERT INTO `correntista` (`id_cliente`, `nome_razao_social`, `email`, `rg_inscricao_social`, `data_nasc_fundacao`, `telefone`, `cep`, `cidade`, `bairro`, `rua`, `num_rua`, `complemento`, `num_conta`, `saldo`) VALUES
(32, 'Sidnei Razao', 'sidnei@email.com', '1544133995', '2022-02-02', 'teste', 'teste', 'teste', 'teste', 'teste', 11, 'teste', 123456789, '0'),
(33, 'Sidnei R', 'sidnei2@email.com', '1544133991', '2022-02-02', 'teste', 'teste', 'teste', 'teste', 'teste', 11, 'teste', 633059543, '4000');

-- --------------------------------------------------------

--
-- Estrutura da tabela `login_correntista`
--

CREATE TABLE `login_correntista` (
  `id_login` int(11) NOT NULL,
  `id_correntista` int(11) NOT NULL,
  `cpf_cnpj` varchar(20) NOT NULL,
  `senha` varchar(300) NOT NULL,
  `data_cadastro` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `login_correntista`
--

INSERT INTO `login_correntista` (`id_login`, `id_correntista`, `cpf_cnpj`, `senha`, `data_cadastro`) VALUES
(5, 32, '071.785.035-09', '$2y$10$Ja2Lv//ETccQdNMbGEPAY.twttsOIBez3puNFJptUcfumfPlI2OkG', '22-07-2022 17:2'),
(6, 33, '070.785.035-09', '$2y$10$y2Ykyqp95HSaXNwnJ98L4.nVPc9MZ2xNoxPmHW5NFrLUFteYqC/7e', '22-07-2022 17:3');

-- --------------------------------------------------------

--
-- Estrutura da tabela `transacoes`
--

CREATE TABLE `transacoes` (
  `id_transacoes` int(11) NOT NULL,
  `nome_remetente` varchar(100) NOT NULL,
  `num_conta_rem` int(9) NOT NULL,
  `valor_transferencia` varchar(10) NOT NULL,
  `id_remetente` int(11) NOT NULL,
  `nome_destinatario` varchar(100) NOT NULL,
  `num_conta_dest` int(9) NOT NULL,
  `data_transacao` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `transacoes`
--

INSERT INTO `transacoes` (`id_transacoes`, `nome_remetente`, `num_conta_rem`, `valor_transferencia`, `id_remetente`, `nome_destinatario`, `num_conta_dest`, `data_transacao`) VALUES
(1, 'teste', 123456789, '1000', 32, 'teste', 633059543, '22-07-2022 20:09:40');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `correntista`
--
ALTER TABLE `correntista`
  ADD PRIMARY KEY (`id_cliente`),
  ADD UNIQUE KEY `num_conta` (`num_conta`);

--
-- Índices para tabela `login_correntista`
--
ALTER TABLE `login_correntista`
  ADD PRIMARY KEY (`id_login`),
  ADD UNIQUE KEY `cpf_uniq` (`cpf_cnpj`),
  ADD KEY `id_pessoa` (`id_correntista`);

--
-- Índices para tabela `transacoes`
--
ALTER TABLE `transacoes`
  ADD PRIMARY KEY (`id_transacoes`,`id_remetente`),
  ADD KEY `fk_id_remetente` (`id_remetente`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `correntista`
--
ALTER TABLE `correntista`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de tabela `login_correntista`
--
ALTER TABLE `login_correntista`
  MODIFY `id_login` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `transacoes`
--
ALTER TABLE `transacoes`
  MODIFY `id_transacoes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `login_correntista`
--
ALTER TABLE `login_correntista`
  ADD CONSTRAINT `fk_id_correntista` FOREIGN KEY (`id_correntista`) REFERENCES `correntista` (`id_cliente`) ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `transacoes`
--
ALTER TABLE `transacoes`
  ADD CONSTRAINT `fk_id_remetente` FOREIGN KEY (`id_remetente`) REFERENCES `correntista` (`id_cliente`) ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
