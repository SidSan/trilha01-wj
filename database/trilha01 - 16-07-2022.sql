-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 16-Jul-2022 às 19:30
-- Versão do servidor: 10.4.24-MariaDB
-- versão do PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `trilha01`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `correntista`
--

CREATE TABLE `correntista` (
  `id_cliente` int(11) NOT NULL,
  `nome_razao_social` varchar(150) NOT NULL,
  `cpf_cnpj` varchar(20) NOT NULL,
  `rg_inscricao_social` varchar(10) NOT NULL,
  `data_nasc_fundacao` date NOT NULL,
  `telefone` varchar(15) NOT NULL,
  `cep` varchar(9) NOT NULL,
  `cidade` varchar(100) NOT NULL,
  `bairro` varchar(100) NOT NULL,
  `rua` varchar(100) NOT NULL,
  `num_rua` int(5) NOT NULL,
  `complemento` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `correntista`
--

INSERT INTO `correntista` (`id_cliente`, `nome_razao_social`, `cpf_cnpj`, `rg_inscricao_social`, `data_nasc_fundacao`, `telefone`, `cep`, `cidade`, `bairro`, `rua`, `num_rua`, `complemento`) VALUES
(1, 'Sidnei R', '070.784.035-09', '070.784.03', '0000-00-00', 'teste', 'teste', 'teste', 'teste', 'teste', 11, 'teste'),
(3, 'Sidnei R', '070.784.035-08', '070.784.03', '2022-02-02', 'teste', 'teste', 'teste', 'teste', 'teste', 11, 'teste'),
(4, 'Sidnei R', '071.785.035-09', '071.785.03', '2022-02-02', 'teste', 'teste', 'teste', 'teste', 'teste', 11, 'teste'),
(6, 'Sidnei Ra', '170.784.035-09', '170.784.03', '2022-02-02', 'teste', 'teste', 'teste', 'teste', 'teste', 11, 'teste'),
(7, 'Sidnei Ra', '171.784.035-09', '171.784.03', '2022-02-02', 'teste', 'teste', 'teste', 'teste', 'teste', 11, 'teste'),
(8, 'Sidnei Ra', '171.784.035-19', '171.784.03', '2022-02-02', 'teste', 'teste', 'teste', 'teste', 'teste', 11, 'teste'),
(9, 'Sidnei Ra', '171.780.035-19', '171.780.03', '2022-02-02', 'teste', 'teste', 'teste', 'teste', 'teste', 11, 'teste'),
(10, 'Sidnei Ra', '171.780.035-00', '171.780.03', '2022-02-02', 'teste', 'teste', 'teste', 'teste', 'teste', 11, 'teste'),
(11, 'Sidnei Ra', '171.700.035-00', '171.700.03', '2022-02-02', 'teste', 'teste', 'teste', 'teste', 'teste', 11, 'teste'),
(12, 'Sidnei Ra', '171.770.035-00', '171.770.03', '2022-02-02', 'teste', 'teste', 'teste', 'teste', 'teste', 11, 'teste'),
(13, 'Sidnei Ra', '11.770.035-00', '11.770.035', '2022-02-02', 'teste', 'teste', 'teste', 'teste', 'teste', 11, 'teste'),
(14, 'Sidnei Ra', '11.770.035-09', '11.770.035', '2022-02-02', 'teste', 'teste', 'teste', 'teste', 'teste', 11, 'teste'),
(16, 'Sidnei R', '070.754.035-09', '070.754.03', '2022-02-02', 'teste', 'teste', 'teste', 'teste', 'teste', 11, 'teste'),
(17, 'Sidnei R', '070.754.035-55', '070.754.03', '2022-02-02', 'teste', 'teste', 'teste', 'teste', 'teste', 11, 'teste'),
(18, 'Sidnei R', '070.771.035-55', '070.771.03', '2022-02-02', 'teste', 'teste', 'teste', 'teste', 'teste', 11, 'teste'),
(19, 'teste', 'teste', 'teste', '2022-02-02', 'teste', 'teste', 'teste', 'teste', 'teste', 11, 'teste'),
(20, 'Sidnei Razao', '324324234', '324324234', '2022-02-02', 'teste', 'teste', 'teste', 'teste', 'teste', 11, 'teste'),
(22, 'Sidnei soares', '324324', '324324', '2022-02-02', 'teste', 'teste', 'teste', 'teste', 'teste', 11, 'teste'),
(23, '22', 'sidnei@email.com', '1233', '0000-00-00', '16-07-2022 19:2', 'teste', 'teste', 'teste', 'teste', 11, 'teste'),
(24, 'Sidnei sa', '32432400', '32432400', '2022-02-02', 'teste', 'teste', 'teste', 'teste', 'teste', 11, 'teste');

-- --------------------------------------------------------

--
-- Estrutura da tabela `login_correntista`
--

CREATE TABLE `login_correntista` (
  `id_login` int(11) NOT NULL,
  `id_correntista` int(11) NOT NULL,
  `email` varchar(320) NOT NULL,
  `senha` varchar(300) NOT NULL,
  `num_conta` int(9) NOT NULL,
  `data_cadastro` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `login_correntista`
--

INSERT INTO `login_correntista` (`id_login`, `id_correntista`, `email`, `senha`, `num_conta`, `data_cadastro`) VALUES
(1, 24, 'sidnei@email.com', '1233', 4000, '16-07-2022 19:2');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `correntista`
--
ALTER TABLE `correntista`
  ADD PRIMARY KEY (`id_cliente`),
  ADD UNIQUE KEY `cpf/cnpj` (`cpf_cnpj`),
  ADD KEY `fk_cpf/cnpj` (`cpf_cnpj`);

--
-- Índices para tabela `login_correntista`
--
ALTER TABLE `login_correntista`
  ADD PRIMARY KEY (`id_login`),
  ADD UNIQUE KEY `num_conta` (`num_conta`),
  ADD KEY `id_pessoa` (`id_correntista`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `correntista`
--
ALTER TABLE `correntista`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de tabela `login_correntista`
--
ALTER TABLE `login_correntista`
  MODIFY `id_login` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `login_correntista`
--
ALTER TABLE `login_correntista`
  ADD CONSTRAINT `fk_id_correntista` FOREIGN KEY (`id_correntista`) REFERENCES `correntista` (`id_cliente`) ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
